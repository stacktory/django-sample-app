from django.contrib.auth import authenticate, login
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseForbidden
from django.shortcuts import redirect
from django.views.generic import TemplateView, View


class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        context = {
            'count': cache.get_many(['black', 'pinto', 'jelly'])
        }
        return self.render_to_response(context)


class CountView(View):
    def post(self, request, *args, **kwargs):
        bean = request.POST['bean']
        count = int(request.POST['count'])
        try:
            cache.incr(bean, count)
        except ValueError:
            cache.set(bean, count)
        return redirect(reverse('home'))


class LoginView(View):
    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(reverse('home'))
            return HttpResponseForbidden('Disabled')
        return HttpResponseForbidden('Invalid')
